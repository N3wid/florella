import java.util.Scanner;
public class question2 {
public static void main (String [] args){

    Scanner sc = new Scanner(System.in);
    System.out.println("Quel est la marque du véhicule ?");        /* demande à l'utilisateur la marque du véhicule */
    String vhmarque = sc.nextLine();        /* mise en place de la variable "vhmarque" */
    System.out.println("Quel est le modèle du véhicule ?");        /* demande à l'utilisateur le modèle du véhicule */
    String vhmodele = sc.nextLine();        /* mise en place de la variable "vhmodele" */
    System.out.println("Le véhicule est-il électrique ? tapez true (vrai) ou false (faux) ");       /* demande à l'utilisateur si le véhicule est électrique */
    boolean elec = sc.nextBoolean();        /* mise en place de la variable "elec" */
    System.out.println("Quel est le prix HT du véhicule ?");        /* demande à l'utilisateur le prix HT du véhicule */
    double prixht = sc.nextDouble();        /* mise en place de la variable "prixht" */
    double tva = 1.2;
    double tvaelec = 1.05;

    if (elec == true) { /* mise en place de la condition : si le véhicule est électrique, tva vaut 5% sinon tva vaut 20% */
        double prixttc = (prixht * tvaelec);
        System.out.println("La marque du véhicule est : "+ vhmarque );      /* affichage de la marque du véhicule */
        System.out.println("Le modèle du véhicule est : "+ vhmodele );      /* affichage du modèle du véhicule */
        System.out.println("Le véhicule est électrique, la tva est de 5%. Le prix TTC est donc de : " + prixttc + (" €"));      
    }
    else{
        double prixttc = (prixht * tva);
        System.out.println("Le véhicule n'est pas électrique, la tva est de 20%. Le prix TTC est donc de : " + prixttc + (" €"));

        }
    }
}
