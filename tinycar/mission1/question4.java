import java.util.Scanner;
public class question4 {
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Quel est la marque du véhicule ?");     /* demande à l'utilisateur la marque du véhicule */
        String vhmarque = sc.nextLine();        /* mise en place de la variable "vhmarque" */
        System.out.println("Quel est le modèle du véhicule ?");     /* demande à l'utilisateur le modèle du véhicule */
        String vhmodele = sc.nextLine();        /* mise en place de la variable "vhmodele" */
        System.out.println("Quel est le prix HT du véhicule ?");        /* demande à l'utilisateur le prix HT du véhicule */
        double prixht = sc.nextDouble();        /* mise en place de la variable "prixht" */
        double tauxtva = 1.6;       
        double prixttc = prixht * tauxtva;     /* formule de calcul pour le prixttc en fonction du prixht et du tauxtva */
        System.out.println("La marque du véhicule est : "+ vhmarque );      /* affichage de la marque du véhicule */
        System.out.println("Le modèle du véhicule est : "+ vhmodele );      /* affichage du modèle du véhicule */
        System.out.println("Le prix TTC est de : "+ prixttc );      /* affichage du prix TTC */
    }
}
