package fr.esiee.javafxtest;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.io.ObjectInputStream;

public class InsertVoyageurFormController {

    @FXML private TextField tfNom;
    @FXML private TextField tfPrenom;
    @FXML private TextField tfAdresse;
    @FXML private TextField tfAnneeNaiss;
    @FXML private TextField tfCodePostal;
    @FXML private TextField tfVille;

    public void validerForm(ActionEvent actionEvent) {
        System.out.println("Le formulaire a bien été validé !");
        System.out.println("");
        System.out.println("Voici les informations saisies :");
        System.out.println();
    }

    public void annulerForm(ActionEvent actionEvent) {

        System.out.println("Le formulaire a bien été annulé !");

    }
}
