package fr.esiee;

import fr.esiee.model.AdressePostale;
import fr.esiee.dao.VoyageurDAO;
import fr.esiee.model.Bagage;
import fr.esiee.model.Voyageur;

import java.sql.SQLException;

public class TestDAO {
    public static void main(String [] args) throws SQLException {
        // Pour tester la méthode que nous avons ajouté
        // Pour test insertAdresse il faut un objet VoyageurDAO et un objet AdressePostale
        VoyageurDAO vDAO = new VoyageurDAO();
        Bagage b = new Bagage(203, "bleu", 20);
        // Tester le service avec la nouvelle adresse
        // int iBagage = vDAO.insertBagage(bg);
        System.out.println("Bagage ajoutée : " + bg);
        // System.out.println("id de la nouvelle adresse : " + vDAO.idAdresse());

    }
}
