package fr.esiee.easyline4;

import java.util.ArrayList;
import java.util.Scanner;

public class Agence {

    private String nom;
    private String adresse;

    // Initialisation d'une ArrayList
    private ArrayList<Voyageur> voyageurs = new ArrayList<>();

    // Initialisation d'un constructeur à 2 arguments
    private Agence(String nom, String adresse){ 
        setNom(nom); 
        setAdresse(adresse); 
    }

    // Initialisation des Getters
    String getName() {return this.nom;}
    String getAdresse() {return this.adresse;}

    ArrayList<Voyageur> getVoyageurs() {
        return this.voyageurs;
    }

    // Initialisation des Setters
    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setVoyageurs(ArrayList<Voyageur> voyageur) {
        this.voyageurs = voyageur;
    }

    // Initialisation de la méthode afficher pour l'affichage de l'agence de voyage
	private void afficher() {
		System.out.println("\n|- Agence de voyage -|" + this);
	}

	// Initialisation du Override pour les carastéristique de l'agence de voyage
	@Override
	public String toString()
	{
		return "\n|-> Nom : " + getName() + "\n|-> Adresse : " + getAdresse();
	}


    void setVoyageur()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Combien êtes vous de voyageur à monter à bord ?");
        int nb = sc.nextInt();


        for (int i=0; i < nb; i++)
        {
            Voyageur voyageur = new Voyageur();
            voyageur.setNom();
            voyageur.setAge();
            voyageur.setAdressePostale();
            voyageur.setBagages();
            voyageur.afficher();

            this.voyageurs.add(voyageur);
            voyageur.afficher();
        }
    }



    // Initialisation de la méthode recherche par le nom du voyageur
    public void rechercheVoyageur(){
        Scanner sc = new Scanner(System.in);
        Voyageur voyageurGet = null;
        boolean recherche = false;

        System.out.println("Quel est le nom du voyageur que vous recherchez ?");
        String searchVoyageur = sc.nextLine();

        for (Voyageur voyageur : this.voyageurs){
            if(voyageur.getNom().equals(searchVoyageur))
            {
                voyageurGet = voyageur;
                System.out.println("Le voyageur: " + voyageur.toString()+ " à été trouvé");
                recherche=true;
            break;

            }
        }
        if (!recherche){
            System.out.println("Le voyageur n'existe pas");
        }

    }

    //Initialisation de la méthode supprimer par le nom du voyageur
    public void deleteVoyageur() {

        Scanner sc = new Scanner(System.in);
        Voyageur supprimerV = null;


        System.out.println("Quel est le nom du voyageur que vous voulez suppimer ?");
        String voyageursupp = sc.nextLine();

        for (Voyageur voyageur : this.voyageurs) {
            if (voyageur.getNom().equals(voyageursupp)) {
                supprimerV = voyageur;

                System.out.println("Voulez-vous vraiment supprimer ce voyageur ?" + voyageur.toString());
                String reponse=sc.nextLine();

                if (reponse.equals("oui")) {
                    voyageurs.remove(voyageur);
                    System.out.println("Le voyageur " + voyageur.toString() + " vient d'être supprimé !");
                    break;
                }

                if (reponse.equals("non")) {
                    System.out.println("On annule la suppression du voyageur " + voyageur.toString());

                }

            }
        }
    }


    // Initialisation d'une 2e méthode afficher
    private void afficheragence(){ 
        System.out.println(this); 
    }

    public static void main(String[] args){
        // Création d'une agence de voyage
        Agence agence = new Agence("Paris", "Martinique");
        agence.afficheragence();

    }
}




















