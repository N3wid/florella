import java.util.Scanner;


public class Compagnie {
    
    public String nom;
    public int code;
    public double ca;
    public String couleur_principale;
    public String couleur_secondaire;

    public String getNom(){ /*Initialisation de l'accesseurs getNom() */
        return this.nom;
    }
    public void setNom(String nom){ /*Création du modificateur setNom */
        this.nom = nom;
    }


    public int getCode(){ /*Initialisation de l'accesseurs getCode() */
        return this.code;
    }
    public void setCode(int code){ /*Création du modificateur setCode */
        this.code = code;
    }


    public double getCa(){ /*Initialisation de l'accesseurs getCa() */
        return this.ca;
    }
    public void setCa(double ca){ /*Création du modificateur setCa*/
        this.ca = ca;
    }


    public String getCouleur_principale(){ /*Initialisation de l'accesseurs getCouleur_principale() */
        return this.couleur_principale;
    }
    public void setCouleur_principale(String couleur_prinpale){ /*Création du modificateur setCouleur_principale */
        this.couleur_principale = couleur_prinpale;
    }


    public String getCouleur_secondaire(){ /*Initialisation de l'accesseurs getCouleur_secondaire() */
        return this.couleur_secondaire + "second"; /*Renvoie, dans l’accesseur getCouleurSecondaire(,) la couleur secondaire suivi de la chaine de caractères "second" */
    }
    public void setCouleur_secondaire(String couleur_secondaire){ /*Création du modificateur setCouleur_secondaire */
        this.couleur_secondaire = couleur_secondaire;
    }

     
    public Compagnie(){

    }
    public Compagnie(String nomCompagnie){
        this.nom = nomCompagnie;
    }

    public Compagnie(String nomCompagnie, String couleurP, String couleurS){ /*Initialisation d'un constructeur à 3 arguments */
        this.nom = nomCompagnie;
        this.couleur_principale = couleurP;
        this.couleur_secondaire = couleurS;
    }

    public Compagnie(String nomCompagnie, String couleurP, String couleurS, int codeCompagnie){ /*Initialisation d'un constructeur à 4 arguments */
        this.nom = nomCompagnie;
        this.couleur_principale = couleurP;
        this.couleur_secondaire = couleurS;
        this.code = codeCompagnie;
    }

    public void afficher(){
        System.out.println("Nom compagnie : " + this.nom);

    }

    public void afficher1(){ /*Modification de la méthode pour afficher que 2 arguments */
        System.out.println("Nom compagnie : " + this.nom);
        System.out.println("Code compagnie : " + this.code);

    }

    public void afficher2(){ /*Modification de la méthode pour afficher que 3 arguments */
        System.out.println("Nom compagnie : " + this.nom);
        System.out.println("Code compagnie : " + this.code);
        System.out.println("Couleur principale de la compagnie : " + this.couleur_principale);
    }

    public void afficher3(){ /*Modification de la méthode pour afficher que 4 arguments */
        System.out.println("Nom compagnie : " + this.nom);
        System.out.println("Code compagnie : " + this.code);
        System.out.println("Couleur principale de la compagnie : " + this.couleur_principale);
        System.out.println("Couleur secondaire de la compagnie : " + this.couleur_secondaire);
    }

    public static void main(String[] args){
        Compagnie maCompagnie1 = new Compagnie();
        maCompagnie1.nom = "zaza";
        maCompagnie1.afficher();
        Compagnie maCompagnie2 = new Compagnie("zozo");
        Compagnie maCompagnie3 = new Compagnie("easyJune"); /*Réalisation d'une nouvelle compagnie */
        maCompagnie3.afficher();/*Afficher la nouvelle compagnie */
        Compagnie maCompagnie4 = new Compagnie("ESIEE-IT"); /*Réalisation d'une nouvelle compagnie avec le constructeur à un argument */
        Compagnie maCompagnie5 = new Compagnie(); /*Réalisation d'une nouvellen compagnie avec le constructeur sans argument */
        maCompagnie5.nom = "Ubisoft";
        System.out.println("");

        maCompagnie1.code = 95; /*Initialisation d'un code pour la compagnie n°1 */
        System.out.println("Code de la compagnie n°1 : " + maCompagnie1.code); /*Afficher le code de la compagnie n°1 */
        maCompagnie2.code = 122;/*Initialisation d'un code pour la compagnie n°2 */
        System.out.println("Code de la compagnie n°2 : " + maCompagnie2.code); /*Afficher le code de la compagnie n°2 */
        System.out.println("");

        maCompagnie1.couleur_principale = "rouge"; /*Initialisation de la couleur principale de la compagnie n°1 */
        System.out.println("Le " + maCompagnie1.couleur_principale + " est la couleur principale de la compagnie " + maCompagnie1.nom); /*Afficher la couleur principale de la compagnie n°1 */
        System.out.println("");

        maCompagnie2.afficher1(); /*Afficher la compagnie n°2 */
        System.out.println("");

        maCompagnie2.couleur_principale = "vert"; /*Initialisation de la couleur principale de la compagnie n°2 */

        maCompagnie3.couleur_principale = "jaune"; /*Initialisation de la couleur principale de la compagnie n°3 */
        maCompagnie3.code = 35; /*Initialisation d'un code pour la compagnie n°3 */
        maCompagnie3.afficher2(); /*Afficher la compagnie n°3 */
        System.out.println("");

        maCompagnie4.couleur_principale = "bleu"; /*Initialisation de la couleur principale de la compagnie n°4 */
        maCompagnie4.code = 972; /*Initialisation d'un code pour la compagnie n°4 */
        maCompagnie4.afficher2(); /*Afficher la compagnie n°4 */
        System.out.println("");

        maCompagnie5.couleur_principale = "violet"; /*Initialisation de la couleur principale de la compagnie n°5 */
        maCompagnie5.code = 360; /*Initialisation d'un code pour la compagnie n°5 */
        maCompagnie5.afficher2(); /*Afficher la compagnie n°5 */
        System.out.println("");

        maCompagnie1.couleur_principale = "orange"; /*Modification de la couleur principale de la compagnie n°1 */
        maCompagnie1.couleur_secondaire = "vert"; /*Initialisation de la couleur secondaire de la compagnie n°1 */
        System.out.println("Le " + maCompagnie1.couleur_principale + " est la nouvelle couleur principale de la compagnie " + maCompagnie1.nom); /*Afficher la nouvelle couleur principale de la compagnie n°1 */
        System.out.println("Le " + maCompagnie1.couleur_secondaire + " est la couleur secondaire de la compagnie " + maCompagnie1.nom); /*Afficher la couleur secondaire de la compagnie n°1 */
        System.out.println("");

        Compagnie maCompagnie6 = new Compagnie("Treyarch", "gris", "orange" ); /*Réalisation d'une nouvelle compagnie avec le constructeur à 3 arguments */
        maCompagnie6.afficher3(); /*Afficher la compagnie n°6 */
        System.out.println("");

        Compagnie maCompagnie7 = new Compagnie("Blizzard", "bleu", "noir", 17 ); /*Réalisation d'une nouvelle compagnie avec le constructeur à 4 arguments */
        maCompagnie7.afficher3(); /*Afficher la compagnie n°7 */
        System.out.println("");
        
        maCompagnie6.code = 30; /*Initialisation d'un code pour la compagnie n°6 */        
        
        Scanner sc=new Scanner(System.in);
        System.out.println("Veuillez saisir le mot de passe !"); /*Initialisation d'un mot de passe afin d'obtenir le code de la compagnie n°6 */
        String mdp=sc.nextLine();
        if(mdp.equals("Superman01")){ /*Initialisation de la condition d'affichage du code de la compagnie n°6 */
            System.out.println("Code de la compagnie n°6 : " + maCompagnie6.code); /*Afficher la compagnie n°6 */
        }else{
            System.out.println("Mot de passe incorrect !");
        }
        System.out.println("");

    }
}
