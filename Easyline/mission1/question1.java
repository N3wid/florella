public class question1 {

    private String nom;

    private int age;

    public void afficher(){

        System.out.println(this.nom + " " + this.age);
    }

    public String toString(){

        return this.nom + " " + this.age;
    }

    //constructeur 1

    public question1(){
    }

    //constructeur 2

     public question1(String nom, int ageD){

        this.nom=nom;

        this.age=ageD;


    }

    public static void main(String args[]){

        //creation d'un objet

        question1 v=new question1("Louis",26);

        System.out.println(v);

       // v.afficher();

       //constructeur 1

       question1 v2=new question1();

       v2.nom="Vincent";

       v2.age=18;

       System.out.println(v2);
    }

}
