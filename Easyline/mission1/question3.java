import java.util.Scanner;

public class question3 {
    
    private String nom;
    private int age;

    public String getNom(){
        return this.nom;
    }

    public void setNom(String n){
        this.nom = n;
    }

    public int getAge(){
        return this.age;      
    }

    public void setAge(int a){
        while(age>0){
            System.out.println("Veuillez ressaisir l'age");
        }
        this.age = a;
    }


    public void afficher(){
        System.out.println(this.nom +" "+this.age);

    }
    public String toString(){
        return this.nom +" "+this.age;

    }
    //constructeur n°1
    public question3(){

    }

    //constructeur n°2
    public question3(String nom, int ageD){
        this.nom = nom;
        this.age = ageD;

    }

    public void categorie(int age){ //initialisation des catégories
        if(age>2){
            System.out.println("Nourrisson");
        }
        else if(age>19){
            System.out.println("Enfant");
        }
        else if(age>61){
            System.out.println("Adulte");
        }
        else {
            System.out.println("Senior");
        }
    }

    public static void main(String arg[]){
        question3 v = new question3("Louis",20);
        System.out.println (v);
        question3 v2 = new question3();
        v2.setNom("Thomas");
        v2.setAge(12);
        System.out.println(v2);
    }

}
