package fr.esiee.easyline.mission3;

public class Voyageur { // initialisation de la classe

    private String nom;
    private int age;
    private AdressePostale AdressePostale;
    private Bagage Bagage;

    public String getNom(){
        return this.nom;
    }

    public void setNom(String n){
        this.nom=n;
    }

    public int getAge(){
        return this.age;
    }

    public void setAge(int a){
        while(age<0){
            System.out.println("Veuillez ressaisir l'age");
        }
        this.age=a;
    }

    public String toString(){
        return this.nom +" "+this.age +" "+ this.AdressePostale;

    }

    //constructeur n°1
    public Voyageur(){
    }

    //constructeur n°2
    public Voyageur(String nom, int ageD) {
        this.nom = nom;
        this.age = ageD;
    }

    //constructeur n°3
    public Voyageur(String nom, int ageD, AdressePostale ap){
        this.nom=nom;
        this.age=ageD;
        this.AdressePostale=ap;
    }

    //constructeur n°4
    public Voyageur(String nom, int ageD, AdressePostale ap, Bagage bg){
        this.nom=nom;
        this.age=ageD;
        this.AdressePostale=ap;
        this.Bagage=bg;

        if(age<2){ //initialisation des catégories
            System.out.println("Nourrisson");
        }
        else if(age<19){
            System.out.println("Enfant");
        }
        else if(age<61){
            System.out.println("Adulte");
        }
        else {
            System.out.println("Senior");
        }

    }

    public static void main(String arg[]){ // afficher voyageur avec nouvelle adresse
        Voyageur v = new Voyageur("Louis",20);
        System.out.println (v);
        Voyageur v2=new Voyageur();
        v2.setNom("Thomas");
        v2.setAge(12);
        System.out.println(v2);

    }

}
