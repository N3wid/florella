package fr.esiee.easyline.mission3;

public class Bagage {
    private String numero;
    private String couleur;
    private String poids;

    /* initialisation du bagage */
    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCouleur() {
        return this.couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getPoids() {
        return this.poids;
    }

    public void setPoids(String poids) {
        this.poids = poids;
    }


    /* constucteur n°1 sans arguements */
    public Bagage(){
    }

    /* constructeur n°2 à 3 arguments */
    public Bagage(String numero, String couleur, String poids){
        this.numero=numero;
        this.couleur=couleur;
        this.poids=poids;
    }

    @Override /* initialisation du OVERRIDE */
    public String toString()
    {
        return "-- Caractéristiques bagage --\n" +
                "Numéro : " + this.numero + "\n" +
                "Couleur : " + this.couleur + "\n" +
                "Poids : " + this.poids + " kg";
    }

    /* afficher le bagage */
    public static void main(String args[]){
        Bagage bg = new Bagage("450", "bleu" , "23");
        System.out.println (bg);
        Bagage bg2=new Bagage(); /*afficher le nouveau bagage en modifiant les arguments*/
        bg2.setNumero("360");
        bg2.setCouleur("rouge");
        bg2.setPoids("12");
        System.out.println(bg2);
    }
}
