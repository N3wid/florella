public class AdressePostale{
    private String voie;
    private String code;
    private String ville;

    /*initialisation de l'adresse postale*/
    public String getVoie(){
        return this.voie;
    }

    public void setVoie(String voie){
        this.voie=voie;  
    }

    public String getCode(){
        return this.code;
    }

    public void setCode(String code){
        this.code=code;
    }
    
    public String getVille(){
        return this.ville;
    }

    public void setVille(String ville){
        this.ville=ville;
    }

    public void afficher(){
        System.out.println(this.voie +" "+ this.code +" "+ this.ville);

    }
    public String toString(){
        return this.voie +" "+ this.code +" "+ this.ville;

    }

    /*constucteur n°1 sans arguements*/
    public AdressePostale(){
        
    }

    /*constructeur n°2 à 3 arguments*/
    public AdressePostale(String voie, String code, String ville){
        this.voie=voie;
        this.code=code;
        this.ville=ville;
    }

    /*afficher l'adresse postale*/
    public static void main(String args[]){
        AdressePostale ap = new AdressePostale("allée des fougères", "95" , "Osny");
        System.out.println (ap);
        AdressePostale ap2=new AdressePostale(); /*afficher nouvelle adresse en modifiant les arguments*/
        ap2.setRue("avenue des airons");
        ap2.setCode("12");
        ap2.setVille("Paris");
        System.out.println(ap2);

    }
}




