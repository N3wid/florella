import java.sql.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //System.out.println("Mission JDBC");
        String urlBDD = "jdbc:mysql://localhost:3306/easyline";
        String user = "root";
        String pwd = "root";
        String reqInsertAdresse = "INSERT INTO AdressePostales (numVoie, ville, codePostal) VALUES ('5 rue de Cergy', 'Osny', '95520')";
        String reqInsertVoyageur = "INSERT INTO Voyageurs (nom, prenom, anneeNaiss, adresse, type) VALUES ('Dewulf', 'Mael', 2003, 1, 'normal')";
        String reqSelectAdresse = "SELECT id, numVoie, codePostal, ville FROM AdressePostales";


        System.out.println("\n| Bonjour jeune voyageur, bienvenue en gare ! |\n");

        Scanner sc = new Scanner(System.in);  // création d'un scranner objet

        System.out.println("Saisissez votre nom !");
        String nomVoyageur = sc.nextLine();
        System.out.println("Saisissez votre prénom !");
        String prenomVoyageur = sc.nextLine();
        System.out.println("Saisissez votre année de naissance !");
        int anneeNaissVoyageur = sc.nextInt();

        System.out.println("\nSaisissez votre adresse !");
        System.out.println("Numéro de la voie : ");
        String numVoie = sc.next();
        System.out.println("Ville :");
        String villeVoyageur = sc.next();
        System.out.println("Code postal :");
        int codePostal = sc.nextInt();
        String adresseVoyageur = (numVoie + " " + villeVoyageur + " " + codePostal);

        System.out.println("\nAvez vous un bagage ?");
        String ibagage = sc.next();

        System.out.println("\n —————————————————");
        System.out.println("| Vos information |");
        System.out.println(" —————————————————");
        System.out.println("Nom : "+ nomVoyageur);
        System.out.println("Prénom : "+ prenomVoyageur);
        System.out.println("Votre année de naissance : "+ anneeNaissVoyageur);
        System.out.println("Votre adresse : "+ adresseVoyageur);
        System.out.println("Bagages : " + ibagage);

        System.out.println("\nEtes vous sur de vos informations ?");
        String check = sc.nextLine();

        System.out.println("Tout est Ok !");
        System.out.println("On vous ajoute à la base de donnée");




        /*try {
            // Récupération d'un objet de type Connexion : son nom "conn"
            Connection conn = DriverManager.getConnection(urlBDD, user, pwd); // Nécessite le driver dans le classpath
            System.out.println("Connection OK.");

            // Requête insertion
            Statement fluxreq = conn.createStatement();

            // Executer requête INSERT via flux - Requête Adresse
            int res = fluxreq.executeUpdate(reqInsertAdresse);
            System.out.println("Nombre de lignes MAJ : " + res);

            // Executer requête INSERT via flux - Requête Voyageur
            //int voy = fluxreq.executeUpdate(reqInsertVoyageur);
            //System.out.println("Nombre de lignes MAJ : " + voy);

            // Executer requête SELECT via flux - Select Adresse
            ResultSet indexRes = fluxreq.executeQuery(reqSelectAdresse);
            while(indexRes.next()){ //Double fonction bouger l'index vers le suivant ; si ça marche return true
                }


        } catch (SQLException e) {
            // throw new RuntimeException(e);
            System.out.println("Problème.");
            e.printStackTrace(); // Affichage de la pile d'exécution qui a causé l'erreur
        }*/
    }
}
