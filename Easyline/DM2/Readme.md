***Vincent Florella | BTS SIO SLAM - 20/02/2023***

---

### Que faire ?

- Quelle classe démarrer ?

Il s'agit de la classe : HelloApplication.java

- Quels fichiers (classes) ont été modifiés ?

Les fichiers modifiés sont présent dans le dossier mission 7 JavaFxTest

Screen des fenêtres résultats (images) :

| Interface Scene bluider |

![Interface Scene bluider](Easyline/DM2/sceneB.png)

| Insert bagage dans la base de donnée|

![Insert bagage dans BDD](Easyline/DM2/insertBDD.png)
