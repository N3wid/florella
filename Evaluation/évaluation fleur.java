public class fleur { // initialisation de la classe fleur
    private String nom;
    private String couleurP;
    private String couleurS;
    private String taille;

    public String getNom(){
        return this.nom;
    }

    public void setNom(String nom){
        this.nom = nom;
    }

    public String getCouleurP(){
        return this.couleurP;
    }

    public void setCouleurP(String couleurP){
        this.couleurP = couleurP;
    }

    public String getCouleurS(){
        return this.couleurS;
    }

    public void setCouleurS(String couleurS){
        this.couleurS = couleurS;
    }

    public String getTaille(){
        return this.taille;
    }

    public void setTaille(String taille){
        this.taille = taille;
    }

    // constructeur n°1 sans argument
    public fleur(){

    }

    // constructeur n°2 à 4 arguments
    public fleur(String nom, String couleurP, String couleurS, String taille){
        this.nom = nom;
        this.couleurP = couleurP;
        this.couleurS = couleurS;
        this.taille = taille;
    }


    public String toString(){
        return this.nom + " de couleur " + this.couleurP + " et " + this.couleurS + ". Elle fait " + this.taille;
    }

    // afficher la fleur
    public static void main(String args[]){
        fleur f = new fleur("Orquidée","bleu","violet","35cm");
        System.out.println(f);
        f.setTaille("23cm");
        System.out.println(f);
    }

}

