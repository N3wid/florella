/* Réaliser un programme de gestion des remboursements d'hospitalisation.*/
/* L'utilisateur saisi ses factures. La sécurité sociale prend en charge 50%, la mutuelle 27%.*/

import java.util.Scanner;
public class exo1{
public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);
    System.out.println("Saisissez le montant de vos frais d'hospitalisation.");
    Double fraisH = sc.nextDouble();

    Double secu = 0.50; 
    Double prixSecu = fraisH * secu;
    System.out.println("La Sécurité Sociale prend en charge 50% de vos frais, soit " + prixSecu + " €");

    Double mutuelle = 0.27;
    Double prixMut = fraisH * mutuelle;
    System.out.println("La mutuelle prend en charge 27% de vos frais, soit " + prixMut + " €");
    Double prixFinal = prixSecu - prixMut;
    System.out.println("Le reste à charge est donc de : " + prixFinal + " €");



    }
}
