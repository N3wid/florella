<?php
require 'vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;
$app->get('/gateaux', function (Request $request, Response $response) {
    return getGateaux(); // appel de la fonction get
});

$app->post('/gateau', function (Request $request, Response $response) {
    $tb = $request->getQueryParams();
    $nom = $tb["nom"];
    $couleur = $tb["couleur"];
    $ingredient_p = $tb["ingredient_p"];
    $date_f = $tb["date_f"];
    $poids = $tb["poids"];

    return postGateau($nom, $couleur, $ingredient_p, $date_f, $poids); // appel de la fonction post
});

function connexion()
{
    return $dbh = new PDO("mysql:host=localhost;dbname=boulangerie", 'root', 'root', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING)); // afficher éventuelles erreur de bd
}
function getGateaux() // initialisation fonction get
{
    $sql = "SELECT * FROM gateau";
    try {
        $dbh = connexion();
        $statement = $dbh->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch (PDOException $e) {
        return '{"error":' . $e->getMessage() . '}';
    }
}


function postGateau($nom, $couleur, $ingredient_p, $date_f, $poids) // initialisation fonction post
{
    $sql = "INSERT INTO `gateau` (`nom`, `couleur`, `ingredient_p`, `date_f`, `poids`)
  VALUES (:nom, :couleur, :ingredient_p, :date_f, :poids)";


    try {
        $dbh = connexion();
        $dbh->errorInfo();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":nom", $nom);
        $statement->bindParam(":couleur", $couleur);
        $statement->bindParam(":ingredient_p", $ingredient_p);
        $statement->bindParam(":date_f", $date_f);
        $statement->bindParam(":poids", $poids);

        $statement->execute();
        return $sql;
        // return json_encode($result, JSON_PRETTY_PRINT);
    } catch (PDOException $e) {
        return '{"error":' . $e->getMessage() . '}';
    }
}


$app->run();
