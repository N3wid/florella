***Vincent FLORELLA - BTS SIO SLAM | 10/02/2023***

---
# | Projet Portfolio

Dans le cadre de l'épreuve E4 du BTS SIO, chaque étudiant doit présenter les compétences acquises tout au long de la formation. Cette présentation personnelle doit être accompagnée d'un site web vitrine contenant l'ensemble des informations concernants ses compétences, réalisations professionnelles en école et en entreprise.

## Description de la stack technique

* Front-end : HTML/CSS/JavaScript
* Desktop : VS Code

## Environnement local

### 🌐 Serveur web

* Nom : Apache
* Version : 2.4
* Port HTTPS : 443

## Environnement de production

* Hébergeur : Alwaysdata
* Type d'hébergement : Dédié
* Datacenter : datacenter Equinix à Paris
* Pays du datacenter : France
* Nom de domaine lié : alwaysdata.net

Le code source du projet se trouve ici :

    "C:\Users\Vincent\Documents\BTS SIO SLAM\Site WEB\Portfolio"

Le projet est accessible ici :

    https://florella.alwaysdata.net/

---

---

# | Projet Rassemblement

On souhaite concevoir un moteur de recherche de héros. Ce moteur de recherche permettra l'affichage de différents héros Marvel ainsi que les informations et caractéristiques les concernants à l'aide de filtres basés sur leur noms ou leurs comics. L'objectif de ce projet est donc de réaliser une application web adaptée à un usage sur écran étroit de type smartphone en utilisant une API avec différents langages. Il est également question de sécuriser l'API avec un système de compte. 

## Description de la stack technique

* Front-end : HTML, CSS, JavaScript, Vuejs
* Back-end : PHP
* Desktop : VS Code, Wamp

## Environnement local

### 🌐 Serveur web

* Nom : Wamp
* Version : 3.2.6
* Port HTTP : 80
* Chemin de l'exécutable : 
* Chemin du fichier de configuration : ../wamp64/www/apimarvel

Démarrage du serveur :

Extinction du serveur :

### 💽 Base de données

* Nom : Données récupérée grâce à l'api officielle Marvel

## Environnement de production

* Hébergeur : 
* Type d'hébergement : 
* Datacenter : 
* Pays du datacenter : 
* Nom de domaine lié :  
* Sauvegarde automatique : 

Le code source du projet se trouve ici :

    C:\wamp64\www\apimarvel    

Le projet est accessible ici :

    https://florella.alwaysdata.net/

---
---

# | Projet NavyMarine

Navy Marine est une compagnie privée de navigation maritime française assurant des liaisons maritimes par bateau depuis les ports continentaux français vers de multiples destinations à travers le monde.
Dans le contexte d’évolution de notre compagnie de navigation maritime, nous avons le souhait de doter nos services d’un outil de qualité. L’objectif de ce projet est de développer une application métier desktop ayant pour but de faciliter et numériser la gestion métier des activités de l’entreprise. En effet, cette gestion métier fonctionne uniquement au format papier et cela devient très contraignant au vu du nombre important de voyageurs voulant profiter de nos services.

## Description de la stack technique

* Front-end : FXML
* Back-end : Java, JavaFX, SQL
* Desktop : JavaFX, IntelliJ, SceneBuilder

## Environnement local

### 🌐 Serveur web

* Nom : Wamp
* Version : 3.2.6
* Port HTTPS : 80

### 💽 Base de données

* Nom : MySQL
* Accès manuel : Adminer
* Version : 4.8.1
* Port : 3306
* Chemin de l'exécutable : 
* Chemin du fichier de configuration : 
* Utilisateur : root    
* Mot de passe : root

Démarrage de la base de données :

    C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Wampserver64

Extinction de la base de données :


## Environnement de production

* Hébergeur : 
* Type d'hébergement : 
* Datacenter : datacenter 
* Pays du datacenter : 
* Nom de domaine lié : 

Le code source du projet se trouve ici :

    C:\Users\Vincent\Documents\BTS SIO SLAM\Mme Abdelmoula\Navy Marine

Le projet est accessible ici :

---
---

# | Projet AP2 (Rugby)

La compétition Rugby Tropical Cup demande les services d'une entreprise spécialisée dans le développement d'applications web pour développer son site web ainsi que des applications web à l'approche de la prochaine compétition de rubgy. Une API est donc utilisé afin d'obtenir les informations de la compétitions.

## Description de la stack technique

* Front-end : Javascript, django
* Back-end : Python, SQL
* Desktop : VS Code

## Environnement local

### 🌐 Serveur web

* Nom : Wamp
* Version : 3.2.6
* Port HTTPS : 80

Démarrage du serveur :

    python manage.py runserver

### 💽 Base de données

* Nom : MySQL
* Accès manuel : Adminer
* Version : 4.8.1
* Port : 3306
* Chemin de l'exécutable : 
* Chemin du fichier de configuration : 
* Utilisateur : root    
* Mot de passe : root

Démarrage de la base de données :

    C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Wampserver64

Extinction de la base de données :


## Environnement de production

* Hébergeur : 
* Type d'hébergement : 
* Datacenter : datacenter 
* Pays du datacenter : 
* Nom de domaine lié : 

Le code source du projet se trouve ici :

    C:\wamp64\www\projet django

Le projet est accessible ici :

---
---

# | Projet Finder

On souhaite réaliser un moteur de recherche d’hôtel. Le moteur de recherche proposera d’indiquer une date de début, une date de fin et une catégorie de chambre. En fonction de ces informations, le moteur affichera une liste de chambres disponibles avec un prix pour les dates sélectionnées et la catégorie choisie. Les valeurs seront obtenues grâce aux API fournies par les hôtels Amor, Byzance, et Caraïbes. On améliorera par la suite les données en sélectionnant d’autres options. On pourra ensuite réaliser une réservation sur l’un des hôtels

## Description de la stack technique

* Front-end : HTML/CSS/Vue
* Back-end : PHP/JavaScript/NodeJS
* Desktop :  VS Code

## Environnement local

### 🌐 Serveur web

* Nom : Wamp
* Version : 3.2.6
* Port HTTP : 80
* Chemin de l'exécutable :

    C:\wamp64\www\finder\connexion.html

### 💽 Base de données

* Nom : MySQL
* Accès manuel : Adminer
* Version : 4.8.1
* Port : 80
* Chemin de l'exécutable :

    C:\wamp64\www\finder\connexion.html

* Utilisateur : root
* Mot de passe : root

Démarrage de la base de données :

## Environnement de production

* Hébergeur : 
* Type d'hébergement : 
* Datacenter : datacenter 
* Pays du datacenter : 
* Nom de domaine lié : 

Le projet est accessible ici 

    C:\wamp64\www\finder
    
Le projet est accessible ici :

---
