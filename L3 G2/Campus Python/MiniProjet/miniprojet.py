from collections import Counter
import re
import csv
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import requests

print("\n\n")
print("   __  ___                                    __ ")
print("  /  |/  (_)__  (_)    ___  _______    (_)__ / /_")
print(" / /|_/ / / _ \/ /    / _ \/ __/ _ \  / / -_) __/")
print("/_/  /_/_/_//_/_/    / .__/_/  \___/_/ /\__/\__/ ")
print("   ___       __     /_/           |___/  ")
print("  / _ \__ __/ /_/ /  ___  ___   ")
print(" / ___/ // / __/ _ \/ _ \/ _ \  ")
print("/_/   \_, /\__/_//_/\___/_//_/  ")
print("       /_/")
print("\n\n")

# Etape n°1
def occurrences_mots(texte):
    mots = re.findall(r'\b\w+\b', texte.lower())
    occurrences = Counter(mots)
    occurrences = occurrences.most_common()
    return occurrences

# Etape n°2
def supprimer_mots_parasites(occurrences, mots_parasites):
    mots_filtrés = [(mot, occ) for mot, occ in occurrences if mot not in mots_parasites]
    return mots_filtrés

# Etape n°3
def lire_mots_parasites(fichier_csv):
    with open(fichier_csv, 'r', newline='') as file:
        reader = csv.reader(file)
        mots_parasites = [mot[0] for mot in reader]
    return mots_parasites

# Étape 4 - [TEST DU CODE]
texte_a_analyser = """
    L'optimisation pour les moteurs de recherche, aussi connue sous le sigle SEO, inclut l'ensemble des techniques qui visent à améliorer le positionnement d'une page, d'un site ou d'une application web dans la page de résultats d'un moteur de recherche.
"""
# Appel de la fonction initialisée dans l'étape 1
occurrences = occurrences_mots(texte_a_analyser)

# Appel de la fonction initialisée dans l'étape 3
mots_parasites = lire_mots_parasites("parasite.csv")

# Appel de la fonction initialisée dans l'étape 2
mots_filtres = supprimer_mots_parasites(occurrences, mots_parasites)

# On affiche les résultats du test
print("\n=== Liste desccurrences de mots triées ===")
for mot, occ in occurrences:
    print(f"{mot}: {occ}")

print("\n=== Liste des mots parasites ===")
print(mots_parasites)

print("\n=== Liste des mots filtrés ===")
for mot, occ in mots_filtres:
    print(f"{mot}: {occ}")


# Etape n°5
def supprimer_balises_html(texte_html):
    soup = BeautifulSoup(texte_html, 'html.parser')
    texte_sans_balises = soup.get_text(separator=' ')
    return texte_sans_balises

# Etape n°6
def valeurs_attribut_html(texte_html, balise, attribut):
    soup = BeautifulSoup(texte_html, 'html.parser')
    valeurs = [element.get(attribut) for element in soup.find_all(balise)]
    return valeurs

# Étape 7 - [TEST DU CODE]

# Test d'utilisation pour les attributs alt des balises <img> et appel de la fonction
html_test_img_alt = """
    <img src='image1.jpg' alt="Description de l'image 1">
    <img src='image2.jpg' alt="Description de l'image 2">
    <img src='image3.jpg' alt="Description de l'image 3">
"""
valeurs_alt_img = valeurs_attribut_html(html_test_img_alt, 'img', 'alt')

# Affichage des résultats pour les attributs alt des balises img
print("\n=== Résultats pour les attributs alt des balises img ===")
for i, valeur in enumerate(valeurs_alt_img, start=1):
    print(f"Image {i}: {valeur}")

# Test d'utilisation pour les attributs href des balises <a> et appel de la fonction
html_test_href = """
    <a href='lien1.html'>Lien 1</a>
    <a href='lien2.html'>Lien 2</a>
    <a href='lien3.html'>Lien 3</a>
"""
valeurs_href_a = valeurs_attribut_html(html_test_href, 'a', 'href')

# Affichage des résultats pour les attributs href des balises a
print("\n=== Résultats pour les attributs href des balises a ===")
for i, valeur in enumerate(valeurs_href_a, start=1):
    print(f"Lien {i}: {valeur}")

# Etape n°8
def extraire_nom_domaine(url):
    parsed_url = urlparse(url)
    return parsed_url.netloc

# Etape n°9
def filtrer_urls_par_domaine(nom_domaine, liste_urls):
    urls_domaine = []
    urls_hors_domaine = []

    for url in liste_urls:
        domaine_url = extraire_nom_domaine(url)
        if domaine_url == nom_domaine:
            urls_domaine.append(url)
        else:
            urls_hors_domaine.append(url)

    return urls_domaine, urls_hors_domaine

# Etape n°10
def recuperer_texte_html(url):
    try:
        reponse = requests.get(url)
        reponse.raise_for_status()
        return reponse.text
    except requests.exceptions.RequestException as e:
        print(f"Erreur lors de la récupération du texte HTML : {e}")
        return None

# Etape n°11
def audit_page():
    # Étape 1 : Demander l'URL de la page à analyser
    print("\n==> Début de l'analyse...")
    url_page = input("Veuillez entrer l'URL de la page à analyser : ")

    # Étape 2 : Récupérer le texte HTML de la page
    texte_html_page = recuperer_texte_html(url_page)

    if texte_html_page:
        # Étape 3 : Supprimer les balises HTML
        texte_sans_balises = supprimer_balises_html(texte_html_page)

        # Étape 4 : Obtenir les occurrences de mots
        occurrences = occurrences_mots(texte_sans_balises)

        # Étape 5 : Lire les mots parasites depuis le fichier CSV
        mots_parasites = lire_mots_parasites("parasite.csv")

        # Étape 6 : Supprimer les mots parasites
        mots_filtres = supprimer_mots_parasites(occurrences, mots_parasites)

        # Étape 7 : Récupérer les valeurs des attributs alt des balises img
        valeurs_alt_img = valeurs_attribut_html(texte_html_page, 'img', 'alt')

        # Étape 8 : Extraire le nom de domaine de l'URL
        nom_domaine = extraire_nom_domaine(url_page)

        # Étape 9 : Créer une liste d'URLs de test
        liste_urls_test = [
            "https://www.example.com/page1.html",
            "https://www.example.com/page2.html",
            "https://www.anotherdomain.com/page3.html",
            "https://www.example.com/page4.html",
        ]

        # Étape 10 : Filtrer les URLs par domaine
        urls_domaine, urls_hors_domaine = filtrer_urls_par_domaine(nom_domaine, liste_urls_test)

        # Affichage des résultats
        print("\n=== Résultats de l'audit ===\n")
        print("Mots clefs les plus importants :", mots_filtres[:3])
        print("Nombre de liens entrants :", len(urls_domaine))
        print("Nombre de liens sortants :", len(urls_hors_domaine))
        print("Présence de balises alt pour les images :", valeurs_alt_img)
        print("\n====== Fin de l'audit ======\n")
    else:
        print("ERROR ! Impossible de récupérer le texte HTML de la page.")

audit_page()

print("   _____           __                         _     __ ")
print("  / __(_)__    ___/ /_ __    ___  _______    (_)__ / /_")
print(" / _// / _ \  / _  / // /   / _ \/ __/ _ \  / / -_) __/")
print("/_/ /_/_//_/  \_,_/\_,_/   / .__/_/  \___/_/ /\__/\__/ ")
print("                          /_/           |___/  ")