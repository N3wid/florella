***Vincent FLORELLA - BTS SIO SLAM***

***31 Mars 2022***

## Mission : Wireshark et découvertes des protocoles

### | Etape 1 - Installer Wireshark |

**1 - Télécharger Wireshark**

- [x] Télécharger ou récupérer le logiciel Wireshark (https://www.wireshark.org/) (Gnu Linux : # apt update et ensuite # apt install wireshark en tant que superutilisateur).

**2 - Résumer en quelques phrases, l'utilité de ce logiciel et ce qu'il permettra de réaliser**

- Wireshark est l’analyseur de protocole. Il peut également être utilisé pour capturer des paquets. Un paquet est simplement une unité de données et Wireshark les capte lorsqu’ils passent de votre appareil à Internet. 

**3 - Installer Wireshark**

- [x] Installer Wireshark sur votre machine hôte (en cochant toutes les options utiles

**4 - Quelles fonctionnalités de wireshark semblent intéressantes ?**

- Wireshark peut analyser les données du fil, via une connexion réseau en direct, ou analyser des fichiers de données à partir de paquets qui ont déjà été capturés. Il peut également capturer le trafic provenant de divers types de médias, tels que Ethernet, LAN, USB, et Bluetooth. De plus, l’outil est également capable de lire les données en direct de toutes sortes de réseaux: Ethernet, IEEE, 802.11, Protocole point à point (PPP)

#### Schéma Diagramme de déploiement minimal : 

Voir [DIAGRAMME MINIMAL](B1_Systeme/img/diagramme_minimal.jpg)

![Déploiement minimal](B1_Systeme/img/diagramme_minimal.jpg)


### | Etape 2 - Analyser le trafic généré par un serveur http |

**1 - Que fait-on pour provoquer l'envoi d'une requête HTTP ? Préciser tous les détails pertinents.**

- Pour provoquer l'envoi d'une requête HTTP, il faut ouvrir le client donc un navigateur web et effectuer une recherche avec : le protocole HTTP sur le port de connexion 80, le domaine et la requête. Il faut ensuite ouvrir Wireshark et capturer le trafic via l'interface de capture en visitant l’URL à partir de laquelle vous voulez capturer le trafic (Ici, un site en http non sécurisé).

**2 - Identifier avant chaque capture l'IP du client et l'IP du serveur.**

- Source Address (IP du client): 192.168.0.29

- Destination Address (IP du serveur): 140.77.168.21

**3, 4 et 5 - Réaliser quelques requêtes HTTP**

- [captureHTTP](captureHTTP.cpapng)

**6 - Filtrer les paquets HTTP échangés**

- Comment différencier les requêtes des réponses ?

Pour différencier les requêtes des réponses, il faut chercher plusieurs éléments. Pour les requêtes, il s'agit de la commande GET avec l'adresse URL du site web entre crochet. Exemple : GET [/ http://florella.alwaysdata.net/]

Pour les réponses, il s'agit du code réponse HTTP. Exemple : HTTP/1.0 200 OK. Le code 200 OK signifie que la requête a réussi et l’objet demandé est à la suite.

- Combien de paires (requête et réponse) ont été échangées suite à votre action décrite en 1. ?

Entre les requêtes et les réponses, deux paires (requête et réponse) ont été échangées.

### | Etape 3 - Analyser le trafic généré par un serveur https |

**1 - Que fait-on pour provoquer l'envoi d'une requête HTTPS ? Préciser tous les détails pertinents.**

- Pour provoquer l'envoi d'une requête HTTPS, il faut ouvrir le client donc un navigateur web et effectuer une recherche avec : le protocole sécurisé HTTPS sur le port de connexion 443, le domaine et la requête. Il faut ensuite ouvrir Wireshark et capturer le trafic via l'interface de capture en visitant l’URL à partir de laquelle vous voulez capturer le trafic (Ici, un site en https sécurisé)

**2 - Identifier avant chaque capture l'IP du client et l'IP du serveur.**

- Source Address (IP du client): 192.168.0.29

- Destination Address (IP du serveur): 52.215.230.6

**3, 4 et 5 - Réaliser quelques requêtes HTTPS**

- [captureHTTPS](captureHTTPS.cpapng)

**6 - Filtrer les paquets HTTPS échangés**

- Comment différencier les requêtes des réponses ?

Pour différencier les requêtes des réponses avec le protocole HTTPS, il s'agit de la même méthode que pour le protocole HTTP mais cette fois-ci, la requête est réalisée de manière sécurisé. On cherhce encore une fois, la commande GET qui signifie que la ressource a été récupérée et est retransmise dans le corps du message.

Pour les réponses, il s'agit du code réponse HTTPS. Exemple : HTTPS/1.0 200 OK. Le code 200 OK signifie que la requête a réussi et l’objet demandé est à la suite.

- Combien de paires (requête et réponse) ont été échangées suite à votre action décrite en 1. ?

Entre les requêtes et les réponses, deux paires (requête et réponse) ont été échangées.

**7 - Quelles différences remarquez-vous avec le protocole HTTP ?**

- La différence que l'on remarques entre les deux protocoles, est que le protocole HTTPS est plus sécurisé grâce au protocole TLS qui protége/sécurise l'envoie des paquets contrairement au protocole HTTP.

### | Etape 4 - Analyser le trafic généré par un serveur FTP |

**1 - Quel type de client faut-il installer pour vous y connecter ? Installez-le !**

- Pour se connecter à un serveur FTP, il faut installer un client ftp. On peut citer par exemple, FileZilla, que l'on va installer.

**2 - Que fait-on pour provoquer l'envoi d'une requête FTP ? Préciser tous les détails pertinents.**

Pour provoquer l'envoi d'une requête FTP, il faut, tout d'abord, aller sur un serveur FTP où l'on s'y connecte avec un login et un mot de passe. On ouvre ensuite FileZilla en entrant les logins/mot de passe du site DLP test, mis à disposotion. On ouvre WireShark et on lance la connexion afin de pouvoir capturer le trafic en mettant le filtre "ftp".

**3 - Identifier avant chaque capture l'IP du client et l'IP du serveur.**

- Source Address (IP du client): 10.49.32.22

- Destination Address (IP du serveur): 35.163.228.146 

**4, 5 et 6 - Réaliser quelques requêtes FTP**

- [captureFTP](captureFTP.cpapng)

**7 - Filtrer les paquets FTP échangés**

- Comment différencier les requêtes des réponses ?

Pour différencier les requêtes des réponses, on peut voir que les réquêtes sont seulement du texte alors que les réponses sont données par le serveur, sous forme de codes.

- Combien de paires (requête et réponse) ont été échangées suite à votre action décrite en 1. ?

Entre les requêtes et les réponses, 3 paires (requête et réponse) ont été échangées.

### | Etape 5 - Analyser le trafic généré par un serveur SSH |

**1 - Quel type de client faut-il installer pour vous y connecter ?** 

Pour se connecter à un serveur SSH et donc à notre espace Alwaysdata, il faut installer un client SSH tel que GitBash.

**2 - Que fait-on pour provoquer l'envoi d'une requête SSH ? Préciser tous les détails pertinents.**

Pour provoquer l'envoi d'une requête SSH, il faut, tout d'abord, ouvrir WireShark et se mettre le filtre "ssh". On lance la capture et, parallèlement, on ouvre GitBash en entrant la commande : `ssh florella@ssh-florella.alwaysdata.net` afin de se connecter à notre espace Alwaysdata.

**3.Identifier avant chaque capture l'IP du client et l'IP du serveur.**

- Source Address (IP du client): 10.49.32.22

- Destination Address (IP du serveur): 185.31.40.87

**4, 5 et 6 - Réaliser quelques requêtes SSH**

- [captureSSH](captureSSH.cpapng)

**7 - Filtrer les paquets SSH échangés**

- Comment différencier les requêtes des réponses ?

ICi, on peut différencier les réquêtes des réponses en regardant d'où provient elles proviennent lors des échanges. En effet, la réquêtes provient de la sources de l'échange et la réponse quant à elle, provient de la destination de l'échange.

- Combien de paires (requête et réponse) ont été échangées suite à votre action décrite en 1. ?

Entre les requêtes et les réponses, 10 paires (requête et réponse) ont été échangées. 

**8 - Que pouvez-vous noter de différent entre les serveurs SSH et FTP ?**

- Ce que l'on peut noter de différent entre ces deux serveurs, c'est que l'un est plus sécurisé que l'autre. En effet, il s'agit du serveur SSH qui est plus sécurisé dans les transfères de fichiers. Pour pallier à cette différence, on peut utilisé le protocole SFTP, qui sécurisé les transfères de fichier en intégrant SSH.

