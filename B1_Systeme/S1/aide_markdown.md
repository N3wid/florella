**|| Vincent FLORELLA ||** **-  27/10/2021**
## Fiche aide commandes markdown

***La tête d'affiche d'une page***
- "#" = titre de niveau 1 ( gros titre )  
- "##" = titre de niveau 2 ( titre moyen )
- "###" = titre de niveau 3 ( petit titre )
- "####" = titre de niveau 4 ( très petit titre )

***Les listes***
- " - [ ... ] " = petit carré en début de ligne 
- " - [x] " = petit carré en début de ligne validé
- " - " = points pour les listes

***Le contenu principal***
- " ( url ) " = lien absolu, depuis la racine du site web
- " [mot] = nom du lien absolu
- "  ```  " = pour afficher chaîne de caractère en police "code"
- " ** ... ** " = texte en gras
- " * ... * " = texte en italique
- " *** ... *** " = texte en gras & en italique
