## Exercices en MS-DOS Windows
#### Vincent FLORELLA
#### 09/12/2021

## Exercice n°1

IMPORTANT : sur Windows, c'est toujours des anti-slach `\`

**Créer un dossier ligne commande dans le dossier courant :**
C:\Users\Vincent> `mkdir ligne-commande`

**Créer les fichier "rep1", "rep2" et "rep3" :**

C:\Users\Vincent\ligne-commande> `mkdir rep1`

C:\Users\Vincent\ligne-commande> `mkdir rep2`

C:\Users\Vincent\ligne-commande> `mkdir rep3`

**Voir ce qui a dans le dossier :**
C:\Users\Vincent> `dir`

**Supprimer un dossier :**
C:\Users\Vincent> `rmdir <chemin/dossier/relatif/ ou /absolu>`

**Modifier nom de dossier ou fichier :**
C:\Users\Vincent> `ren <chemin/dossier/relatif/ ou /absolu/source> <chemin/dossier/ou/fichier/relatif/ou/absolu/dest>`

**Modifier le fichier "fichier22.txt" :**

C:\Users\Vincent\ligne-commande\rep1> `cd ..\`

C:\Users\Vincent\ligne-commande> `cd rep2`

C:\Users\Vincent\ligne-commande>\rep1> `echo Linux >> fichier22.txt`

**Creér les fichiers "rep31" "rep32" dans le rep3 :**

C:\Users\Vincent\ligne-commande\rep2> `cd..\`

C:\Users\Vincent\ligne-commande> `cd rep3`

C:\Users\Vincent\ligne-commande> `mkdir rep31`

C:\Users\Vincent\ligne-commande\rep3> `mkdir rep32`

## Exercice n°2

**Créer un dossier exo2 :**

C:\Users\Vincent> `mkdir exo2`

**Se déplacer dans le dossier exo2 :**
C:\Users\Vincent> `cd exo2`

**Créer le fichier "moi.txt" :**
C:\Users\Vincent\exo2> `type nul > moi.txt`

**Afficher le message "Je suis en BTS SIO" :**
C:\Users\Vincent\exo2> `echo "Je suis en BTS SIO" >> moi.txt`

**Ajouter dans le fichier moi.txt la ligne "Et j’aime ça !" :**
C:\Users\Vincent\exo2> `echo "Et j'aime ça" >> moi.txt`

**Vous déplacer à la racine de la partition :**
C:\Users\Vincent\exo2> `../../` 

## Exercice n°3

**Créer un dossier exo3 :**


C:\Users\Vincent> `mkdir exo3`

**Se déplacer dans le dossier exo3 :**
C:\Users\Vincent> `cd exo3`

**Créer le fichier "moi.txt" :**
C:\Users\Vincent\exo3> `type nul > moi.txt`

**Ajouter le message "Je suis en BTS SIO" :**
C:\Users\Vincent\exo3> `echo Je suis en BTS SIO >> moi.txt`

**Rajouter le message "Et j'aime ca !" :**
C:\Users\Vincent\exo3> `echo Et j'aime ca ! >> moi.txt`

**Créer le fichier presidents.txt :**
C:\Users\Vincent\exo3> `type nul > presidents.txt`

**Rajouter "Francois Mitterand", "Jacques Chirac", "Georges Pompidou" :**

C:\Users\Vincent\exo3> `echo Francois Mitterand >> presidents.txt`

C:\Users\Vincent\exo3> `echo Jacques Chirac >> presidents.txt`

C:\Users\Vincent\exo3> `echo Georges Pompidou >> presidents.txt`

**Créer le fichier rio.txt :**
C:\Users\Vincent\exo3> `type nul > rio.txt`

**Rajouter "Rio de Janeiro est la deuxième plus grande ville du Brésil." :**

C:\Users\Vincent\exo3> `echo Rio de Janeiro est la deuxième plus grande ville du Brésil. >> rio.txt`

**En une seule commande**

## Exercice n°4
#### Trouvez comment faire pour :

**Créer un dossier "bin" dans le répertoire personnel :**
C:\Users\Vincent> `mkdir bin`

**Afficher la liste des variables d’environnement :**
C:\Users\Vincent\bin> `set`

**Afficher le nom de l’utilisateur courant :**
C:\Users\Vincent\bin> `net user`

#### Modifiez votre environnement pour : 

**Ajouter le répertoire bin créé précédemment à la variable PATH :**

C:\Users\Vincent\bin> `>set PATH=%PATH%;C:\Document\bin`

C:> `path`
