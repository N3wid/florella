# Description du matériel de mon STA personnel
**|| Vincent FLORELLA || - 27/10/2021**
## La partie externe de l'ordianteur
**La marque :** Ordinateur portable Lenevo Legion Y520

**Les composants essentiels pour le bon fonctionnement de la STA et leurs caractéristiques :**
- CPU Intel(R) Core i5-7300HQ CPU @ 2.50GHz
- GPU NVIDIA GeForce GTX 1050 2GB
- Mémoire ramaxel 8.0 GB
- Disque dur 128 GB + SSD 1 TB

**Les connecteurs disponibles :** 
- Un port usb type C
- Un port carte SD
- Un port usb 3.0
- 2 ports usb type A
- Un port HDMI
- Une prise jack
- Un port ethernet

## La partie interne de l'ordianteur

**Le système d'exploitation et ses caractéristiques :**
- Windows 10 Famille - 64 bits, processeur x64

**Logiciels installés (version, type de licence) :**

<details><summary>Les jeux :</summary>

- Apex Legends

- Assassin's Creed Sydicate 

- Brawlhalla

- Ghostrunner (version > 40433_436)

- Paladins

- Fortnite 

- Rocket league 

- Rogue compagny

- Warfame
</details>

<details><summary>Les logiciels :</summary>

- Battle.net

- Discord (version --> 0.0.310)

- Epic Games Launcher (version --> 1.1.279.0)

- GOG GALAXY

- Java 8 Update 301 (version --> 8.0.3010.9)

- Visual Studio Code (version --> 1.61.0)

- Python 3.10.0 (version --> 3.10.150.0)

- Spotify (version --> 1.169.612.gb7409abc)

- Steam (version --> 2.10.91.91)

- Ubisoft Connect (version --> 104.0)

- VLC media player (version --> 3.0.11)
</details>


**Possibilités d'évolution matériel ou logiciel :**

```Possibilité d'ajouter un SSD ainsi que de modifier la mémoire originelle``` 


![Vue d'ensemble du pc](img/Lenovo_y520.jpg)

