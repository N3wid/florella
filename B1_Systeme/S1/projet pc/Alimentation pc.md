# Alimentation

**Qu'est ce qu'un bloc d'alimentation ?**

> Un ordinateur se compose de plusieurs éléments matériels en intéraction. Le bloc d’alimentation, ou simplement l'alimentation, d'un PC est le matériel informatique l'alimentant. L’alimentation est compatibles avec les circuits électroniques de l’ordinateur.


![Alimentation d'un ordinateur](B1_Systeme/img/alimentation_pc.jpg)

**Fonctionnement**

> L’alimentation fournit du courant électrique à l’ensemble des composants de l’ordinateur. Le bloc d’alimentation doit posséder une puissance suffisante pour alimenter les différents périphériques de ce dernier.

**Tensions et connectique**

> Il existe deux types de brochages principaux : AT et ATX.

- AT : C’est un format d’alimentation à découpage utilisé dans les ordinateurs PC de type Pentium et antérieur. Dans ces alimentations, l’interrupteur de mise en service est directement branché sur le réseau électrique.

![Alimentation AT](B1_Systeme/img/Capture_AT.JPG)

- ATX : C’est le format d’alimentation à découpage utilisé dans les ordinateurs PC de type Pentium II et postérieur. Dans ces alimentations, l’interrupteur de mise en service est connecté sur la carte mère, le réseau électrique est connecté en permanence, avec parfois un interrupteur de sécurité pour la maintenance.

![Alimentation ATX](B1_Systeme/img/Capture_ATX.JPG)


#### Les contraintes techniques d'un bloc d'alimentation

**Le rendement** 

> Le rendement d’une alimentation est très important. Il s’agit du rapport entre la puissance délivrée aux composants et la puissance tirée de la prise électrique. Elle doit transformer le courant électrique alternatif du secteur en courant électrique continu que les composants du PC peuvent utiliser. Il est conseillé d'avoir une alimentation à fort rendement. Ce type d'alimentation génère moins de déperditions d'énergie pour le bloc d'alimentation. En effet, si le bloc d'alimentation chauffe trop, les composants peuvent être abîmés et donc plus fonctionnels.

**La puissance**

> La plupart du temps, les blocs indiquent en Watts non pas la puissance qu'ils peuvent fournir en sortie, mais la puissance absorbée au niveau de la prise de courant. En fonction du rendement du bloc d'alimentation, la puissance fournie en sortie sera plus ou moins importante. Par exemple une alimentation bas de gamme au mauvais rendement, aura besoin de 300W absorbé pour fournir 210W en sortie. Presque seuls les blocs industriels et hauts de gamme, ou de marque connue, indiquent clairement (toujours en Watts) la puissance réelle fournie.






