***Vincent FLORELLA - BTS SIO SLAM***

***16 Mars 2022***

## Mission : Déploiement sur un serveur distant avec AlwaysData

### | Etape 0 : Créer un espace sur Alwaysdata |

**Quels services sont offerts par Alwaysdata ?**

- Les service offerts par AlwaysData sont : l'hébergement de données telles que des e-mails, des sites web, des médias, des bases de données, du support TLS. Il y a, de plus, un accès distant sur leurs serveurs.

**Quels services seront nécessaires pour déployer un site web (HTML, CSS, JS et PHP) ?**

- Les services nécessaires pour déployer un site web sont : la base de donnée, le domaine ainsi que l'accès distant

**Quel est le nom de domaine choisi pour le déploiement de votre site ?**

- Il s'agit de : *ssh-florella.alwaysdata.net*

### | Etape 1 : Activer SSH |

**Expliquer l'intérêt du protocole SSH. Sur quel port est-il actif par défaut ?**

- L'intérêt du protocole **SSH** est d'accéder à distance, de manière sécurisée, à un serveur distant. C'est sur le port TCP 22, que le protocole SSH est actif par défaut.

**Quel autre protocole semble avoir les mêmes fonctionnalités ? Que fait SSH qui n'est pas possible avec le 2e ?**

- Il s'agit du protocole **FTP**. Contrairement au protocole FTP, le protocole SSH peut administrer à distance une machine. On parle ici de : créer des fichiers ou encore récupérer des lignes de commandes.

**Activer un accès au serveur via ce protocole. Quelles étapes sont nécessaires ?**

- Pour activer un accès au serveur via ce protocole, il faut :

Aller sur AlwaysData --> Aller sur Accès distant --> Aller sur SSH --> Activer la connexion par mot de passe et définir un mot de passe

**Se connecter à votre espace dédié sur le serveur via ce protocole. Quelle est la ligne de commande nécessaire pour y arriver ?**

- Il s'agit de : *ssh florella@ssh-florella.alwaysdata.net*

**Dans quel répertoire faut-il déposer vos fichiers du site si vous voulez le voir en ligne ?** (chemin complet sur le serveur)

- Pour déposer nos fichiers du site afin de le voir en ligne, il faut se connecter à notre espace dédié syr le serveur puis se rendre sur le répertoire suivant : */home/florella/www*

### | Etape 2 : Copier notre contenu sur Alwaysdata |

**Quel est le chemin local absolu pour accéder à votre site ?**

- Il s'agit de : */home/florella/www*

**Quel est le chemin absolu du répertoire dédié sur le serveur Alwaysdata ?**

- Il s'agit de : */home/florella/*

**Les commandes scp et rsync peuvent être d'une grande aide à cette étape. Pourquoi ?**

- Les commandes scp et rsync sont une grande aide à cette étape, car il faut copier le contenu de notre site sur AlwaysData. En effet, la commande rsync est utile pour la synchronisation des fichiers. La commande scp est utile pour le transfert de fichiers.

**Quelle est la différence entre les deux commandes ?**

- La commande rsync offre davantage d'options contrairement à la commande scp. Elle ne copie pas les fichiers qui ont déjà été copié. La commande scp permet de copier seulement à distance les fichiers.

**Quelle est la commande complète pour ajouter les fichiers sauvegardés en local sur le serveur dédié ?**

- Il s'agit de : scp root@192.56.1:/florella/www/fichier/home/florella/data/

**Comment vérifier que l'ajout a bien été effectué ? Détailler la procédure et les résultats attendus.**

- Afin de vérifier que l'ajout a bien été effectué, je me rends dans le répertoire correspondant et je fais la commande *ls* pour voir ce qu'il y a dans le fichier. Si j'obtiens le résultat attendu, c'est-à-dire, les fichiers de mon site alors, l'ajout a bien été effectué

**Quelle URL permet de voir votre site en ligne ?**

- Il s'agit de : *https://florella.alwaysdata.net/*
