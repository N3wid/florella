***Vincent FLORELLA - BTS SIO SLAM***

***12 Mai 2022***

## Mission OWASP (Open Web Application Security Project)

### | Vulnérabilité n°1 : Contrôle d'accès défaillant 

Le contrôle d'accès est un mécanisme qui permet de vérifier qu'un utilisateur accède uniquement à certaines ressources. Cependant, les restrictions sur les droits des utilisateurs authentifiés sont souvent mal appliquées. Les attaquants peuvent alors, exploiter ces failles pour accéder à des données et/ou fonctionnalités non autorisées. Les conséquences peuvent être graves comme le vol d'identité et la rectification de droit d'accès ou d'information privée. 

OWASP recommande alors : 

- Réfléchir sur le fonctionnement du contrôle d'accès au début de la conception de l'application.
- Toutes les demandes doivent passer par un contrôle d'accès.
- Appliquer le principe du moindre privilège. C'est-à-dire, avoir un compte différent pour avoir des accès administrateur.

En tant que développeur, nous sommes évidemment concernés.

### | Vulnérabilité n°2 : Défaillances cryptographiques et expositions de données sensibles 

Beaucoup d'applications web et d'APIs ne protègent pas correctement les données sensibles telles que les données bancaires, les données relatives aux soins de santé, les données personnelles d'identification. Les pirates peuvent alors voler ou modifier ces données mal protégées pour effectuer une usurpation d'identité, une fraude à la carte de crédit ou d'autres crimes.

OWASP recommande alors 3 grands principes : 

- Classifier les données pour savoir quelles sont les données les plus sensibles à chiffrer.
- Chiffrer les données lors du transport ou chiffrement des données transmises.
- Chiffrer les données sensibles lors du stockage.

En tant que développeur, nous sommes évidemment concernés.

### | Vulnérabilité n°3 : Injection

Une faille de type injection se produit quand une donnée non fiable est envoyée à un interpréteur en tant qu'élément d'une commande ou d'une requête. Les données hostile de l'attaquant peuvent duper l'interpréteur afin de l'amener à des données non autorisées.

OWASP recommande alors : 

- Vérifier systématiquement les données entrées par un utilisateur et protéger les caractères spéciaux.

En tant que développeur, nous sommes évidemment concernés.

### | Vulnérabilité n°4 : Conception non sécurisée

Une conception non sécurisée, expose le code d'une application à certaines violations par un pirate.

OWASP recommande alors :

- Intégrer la sécurité dans le cycle de vie du développement d'une application.

En tant que développeur, nous sommes évidemment concernés.

### | Vulnérabilité n°5 : Mauvaise configuration de sécurité

La mauvaise configuration de sécurité est le résultat de configuration par défaut non sécurisée, de configuration incomplète, d'un stockage dans un cloud ouvert, d'en-têtes HTTP mal configurés et de messages d'erreur verbeux contenant des informations sensibles.

OWASP recommande alors :

- Avoir une procédure de durcissement des systèmes d'exploitation.
- Avoir une procédure de vérification des permissions.
- Désactiver les services qui ne sont pas utilisés.

En tant que développeur, nous sommes évidemment concernés.

### | Vulnérabilité n°6 : Composants vulnérables et obsolètes

Lors de la conception d'une application, un développeur peut utiliser des composants obsolètes ou vulnérables comme un OS ou une base de données qui n'est plus à jour. Ces éléments constituent une porte d'entrée pour un pirate.

OWASP recommande alors :

- Télécharger les composants depuis une source fiable.
- Mettre à jour régulièrement les composants.
- Désactiver les composants que l'on n'utilise pas.
- Suivre régulièrement les publications sur les vulnérabilités.

En tant que développeur, nous sommes évidemment concernés.

### | Vulnérabilité n°7 : Identification et authentification de mauvaise qualité

Une application qui ne gère pas bien les authentifications et les sessions des utilisateurs constituent un danger pour le SI. Un utilisateur qui possède des droits supérieurs à ce qu'il devrait posséder par exemple. On peut aussi prendre l'exemple d'une application qui ne bloque pas l'accès à l'authentification après plusieurs tentatives échouées, un pirate pourrait en profiter pour accéder au SI avec un nombre illimités de tentatives.

OWASP recommande alors :

- Changer le mot de passe par défaut.
- Activer l'authentification à multifacteur (MFA).
- Journaliser les tentatives d'authentification.
- Mise en place d'une politique de mot de passe.
- Mise en place un temporisateur après une succession d'authentification échouée.

En tant que développeur, nous sommes évidemment concernés.

### | Vulnérabilité n°8 : Manque d'intégrité des données et du logiciel

Une application qui s'appuie sur une bibliothèque, un module venant d'une source non fiable. Téléchargements de mise à jour sans vérifier l'intégrité des fichiers.

OWASP recommande alors :

- Vérifier l'intégrité des fichiers avant la mise à jour.
- Interdire la désérialisation de données provenant d'une source non fiable.

En tant que développeur, nous sommes évidemment concernés.

### | Vulnérabilité n°9 : Supervision et journalisation insuffisantes

Détection difficile d'incidents, d'anomalies ou d'incidents.

OWASP recommande alors :

- Envoyer ses fichiers vers un serveur de log.
- Utiliser un outil de supervision.
- Procédure de réponse d'incidents.

En tant que développeur, nous sommes évidemment concernés.

### | Vulnérabilité n°10 : Falsification de requête côté serveur (SSRF = Server-side Request Forgery)

Forcer une application à envoyer des requêtes vers un domaine ou un ordinateur spécifique.

OWASP recommande alors :

- Journaliser le trafic.
- Vérifier les informations envoyées par un utilisateur.
- Utiliser une liste blanche contenant les protocoles et les adresses IP autorisées.

En tant que développeur, nous sommes évidemment concernés.

### | Lexique difficile

- API : interface logicielle qui permet de « connecter » un logiciel ou un service à un autre logiciel ou service afin d'échanger des données et des fonctionnalités.

- Temporisateur : Qui vise à différer le moment d'agir 

- Sérialisation : prendre des objets dans le code de l'application et à les convertir dans un format utilisable plus tard par exemple le stockage de données sur le disque.

- Désérialisation : Reconvertir les données sérialisées en objet utilisable par une application.

