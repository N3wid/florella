***Vincent FLORELLA - BTS SIO SLAM***

***31 Mars 2022***

## Mission : Installation Mutillidae

#### Définition :
**Mutillidae est un site web conçu pour identifier et tester les failles de sécurité. De plus, il est possible de déterminer le niveau de sécurité appliqué pour chacunes des failles.**

**- 1 : Connexion à notre espace Alwaysdata et SSH via Gitbash**

![Connexion SSH](B1_Systeme/img/etape1.png)

**- 2 : Clone Mutillidae**

Tout d'abord, il faut se rendre sur le dossier prévu pour les applications web : `/home/user/www` pour ensuite pourvoir cloner le dépôt et installer Mutillidae au bon endroit : `git clone https://github.com/webpwnized/mutillidae.git`. Un dossier va donc être créé et accessible via : `https://florella.alwaysdata.net/mutillidae/`

![Installation](B1_Systeme/img/etape3.png)

**- 3 : Ouvrir l'accès à Mutillidae**

L'application web est cependant bloqué. En effet, on ne doit pas laisser sur le web, l'accès à une application web aussi défaillante. OWASP a donc bloqué les accès des clients extérieurs (accès via internet)

![Ouvrir la porte](B1_Systeme/img/etape2.png)

**- 4 : Création d'une base de donnée et d'un utilissateur**

On créer par la suite une base de données MySQL dédiée et un utilisateur sur le profil Alwaysdata :
- Nom Base de donnée : florella_mutillidae
- Utilisateur Base de donnée : florella_mutilli
- Mot de passe : m*********

![BDD](B1_Systeme/img/IM5.png)
![user BDD](B1_Systeme/img/IM6.png)

**- 5 : Modifications de la base de donnée**
Il faut maintenant compléter correctement le fichier de configuration suivant : `/home/user/www/mutillidae/includes/database-config.inc`. Pour cela, on entre la commande : `nano database-config.inc`

![Modifications](B1_Systeme/img/etape4.png)

**- 6 : Utilisation de Mutillidae**

Une fois toutes les étapes compléter, l'application web Mutillidae est enfin utilisable 

![Mutillidae](B1_Systeme/img/etape_final.png)

**Il ne faut pas oublier de rebloquer l'application à la fin de chaque utilisation en renommant le fichier no.htaccess en .htaccess !**
