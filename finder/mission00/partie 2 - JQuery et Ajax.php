<!-- CONNEXION -->

<?php session_start();?>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<?php
/*
if(isset($_SESSION['erreur']) && $_SESSION['erreur'] ){
  //echo "erreur";
  echo '<script>
    $( document ).ready(function() {
    $(".form-control").addClass("is-invalid");
    });
    </script>';
}
$_SESSION['erreur']=false;
*/
?>

<div class="container">
    <div class="form-group">
      <label for="exampleInputEmail1">Adresse mail</label>
      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entrer votre mail !">
       <div class="invalid-feedback">
          utilisateur et/ou mot de passe invalide
        </div>
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Entrer votre mot de passe !">
    </div>
    
    <button  class="btn btn-primary">Connexion</button>
  
</div>
<div id="result"></div>

<script>
$(document).ready(function () {

    $('.btn').click(function () {
        $("#result").html("");
        let email=$( "#exampleInputEmail1" ).val();
        let password=$( "#exampleInputPassword1" ).val();
        $.ajax("http://localhost/finder00/verification1.php",
            {
                type: "GET",
                data: 'email=' + email + '&password=' + password,
                success: function (data, textStatus, jqXHR) {
                    console.log(jqXHR.status);
                    $("#result").html(jqXHR.status);

                },
                error: function (xhr,status,error) {
                    console.log(xhr.status);
                    $("#result").html(xhr.status);

                }
                
            });
    });
});
</script>

</html>

<!-- VERIFICATION -->

<?php session_start();

if (
    isset($_GET['email'])
    && isset($_GET['password'])
) {
    //echo "ok";
    $dsn = 'mysql:dbname=finder;host=127.0.0.1';
    $user = 'root';
    $password = 'root';
    try {
        $dbh = new PDO($dsn, $user, $password);
    } catch (PDOException $e) {
        echo 'Connexion échouée:' . $e->getMessage();
    }
    $sql = "SELECT count(*) FROM user WHERE email=:email AND password=TO_BASE64(AES_ENCRYPT(:password,SHA2('boom',256)))";
    //echo $sql;
    $resultats = $dbh->prepare($sql);
    $email = $_GET['email'];
    $password = $_GET['password'];
    $resultats->bindParam(":email", $email);
    $resultats->bindParam(":password", $password);
    $resultats->execute();
    $number_of_rows = $resultats->fetchColumn();
    //echo $number_of_rows;

    if ($number_of_rows == 1) {
        http_response_code(200);
    } else {
        http_response_code(403);
        //$_SESSION['erreur']=true;
        //header('Location: http://localhost/finder1/connexion.php');
    }
} else {
    http_response_code(403);
    //$_SESSION['erreur']=true;
    //header('Location: http://localhost/finder1/connexion.php');
}
